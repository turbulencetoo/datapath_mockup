use std::fs;
use std::io::Read;

use bits::{BLOCK_SIZE, PLB_SIZE, NUM_PLBS};
use data_type::DataType;

const DATA_DIR:   &str = "data";
const TOPS_DIR:   &str = "data/tops";
const MIDS_DIR:   &str = "data/mids";
const LEAVES_DIR: &str = "data/leaves";
const VLBS_DIR:   &str = "data/vlbs";
const PLBS_DIR:   &str = "data/plbs";

fn get_dir(dtype: DataType) -> &'static str {
    match dtype {
        DataType::Top  => TOPS_DIR,
        DataType::Mid  => MIDS_DIR,
        DataType::Leaf => LEAVES_DIR,
        DataType::Vlb  => VLBS_DIR,
        DataType::Plb  => PLBS_DIR
    }
}

// Each location on disk is just a file with a numerical name...
//  so the MID at index 5 is just a file called data/mids/5
// PLBs are special because they have offsets that can be written independently:
//   PLB is a directory and an offset is a file. Thus the data at plb 8, offset 19 is at data/plbs/8/19
fn get_file_path(dtype: DataType, idx: u32, plb_offset: Option<u32>) -> String {
    let mut path = format!("{}/{}", get_dir(dtype), idx);
    if dtype == DataType::Plb {
        path = format!("{}/{}", path, plb_offset.unwrap());
    }
    path
}

pub fn read_data(dtype: DataType, idx: u32, plb_offset: Option<u32>) -> Vec<u8> {
    let mut contents = Vec::new();
    let path = get_file_path(dtype, idx, plb_offset);

    let mut file = fs::File::open(&path).unwrap_or_else(|_| panic!("Cant find {}", path));
    file.read_to_end(&mut contents).unwrap();

    contents
}

pub fn write_data(data: &[u8], dtype: DataType, idx: u32, plb_offset: Option<u32>) {
    let path = get_file_path(dtype, idx, plb_offset);
    fs::write(path, &data).unwrap();
}

pub fn init_dirs() {
    let dirs = [DATA_DIR,
                TOPS_DIR,
                MIDS_DIR,
                LEAVES_DIR,
                VLBS_DIR,
                PLBS_DIR];
    for dir in &dirs {
        // here an error is OK if its because dir already exists
        fs::create_dir(dir).ok();
    }
    // create a bunch of plbs
    for i in 0..NUM_PLBS {
        create_empty_plb(i);
    }
}

//a plb is a dir with a bunch of files that represent offests
fn create_empty_plb(idx: u32) {
    let loc = format!("{}/{}", PLBS_DIR, idx);
    fs::create_dir(&loc).unwrap();

    // zero the whole PLB
    let data = vec![0u8; BLOCK_SIZE as usize];
    let num_blocks = PLB_SIZE/BLOCK_SIZE;
    for offset in 0..num_blocks {
        fs::write(&format!("{}/{}", loc, offset), &data).unwrap();
    }
}

pub fn clean() {
    fs::remove_dir_all(DATA_DIR).unwrap();
}