use data_type::DataType;
use bits;
use backend;
use node::{Node, VLB, Block};

// TODO this is important
fn get_free_plb() -> u32 {
    12
}


#[derive(Serialize, Deserialize)]
struct Tree {
    top: u32, // id of the singleton top to read from disk... TODO is this dumb
    next_mid: u32,  // These indicate what idx to create when making the next mid or leaf
    next_leaf: u32,
    next_vlb: u32
}

impl Tree {
    fn new() -> Self {
        // make sure our singleton top exists
        let top = Node::new(DataType::Top, 0); // Singleton top has id 0
        top.write();
        Tree {top: 0, next_mid: 0, next_leaf: 0, next_vlb: 0}
    }
    fn write(&mut self, lba: u32, data: &[u8]) {
        assert!(data.len() <= bits::BLOCK_SIZE as usize);

        let mut top_node = Node::read(DataType::Top, self.top);

        // Handle top->mid connection
        let mut mid_node  = top_node.get_child_or_make(lba, &mut self.next_mid);

        // Handle mid->leaf connection
        let mut leaf_node = mid_node.get_child_or_make(lba, &mut self.next_leaf);

        // Handle leaf-> VLB connection
        let plb_idx: u32 = get_free_plb();
        let vlb_idx = self.next_vlb;
        self.next_vlb += 1;
        let mut vlb = VLB::new(vlb_idx, plb_idx);
        leaf_node.set(lba, vlb_idx);
        leaf_node.write();

        // Handle VLB -> PLB Connection
        vlb.set(lba, Block::new(0, 4096, 1)); // TODO Magic numbers... OFFSET ALWAYS 0...
        vlb.write(vlb_idx);

        // Write data to the PLB on disk
        backend::write_data(data, DataType::Plb, plb_idx, Some(0)); // OFFSET ALWAYS 0...
    }

    fn read(&self, lba: u32) -> Vec<u8> {
        let top_node = Node::read(DataType::Top, self.top);
        let mid_node  = top_node.get_child_or_panic(lba);
        let leaf_node = mid_node.get_child_or_panic(lba);
        let vlb = VLB::read(leaf_node.get(lba).unwrap());
        backend::read_data(DataType::Plb, vlb.on_disk.plb_idx, Some(0)) // OFFSET ALWAYS 0...
    }
}


pub fn test() {
    backend::clean();
    backend::init_dirs();
    let mut tree = Tree::new();
    tree.write(2, &[65, 32, 65, 32]);
    println!("{:?}", tree.read(2));
}