use std::collections::HashMap;

use bincode::{serialize, deserialize};

use bits;
use backend;
use data_type::DataType;



// Class that represents a serializable top mid or leaf that will be written to disk
// The class instance should know its own index and level in the tree, but that data
//  not be serialized
pub struct Node {
    level: DataType,
    idx: u32,
    lookup: NodeOnDisk
}

#[derive(Debug, Serialize, Deserialize)]
struct NodeOnDisk {
    array: [u32; bits::NUM_NODE_PTRS]
}

impl Node {
    pub fn new(level: DataType, idx: u32) -> Self {
        Node { level,
               idx,
               lookup: NodeOnDisk { array: [bits::SENTINEL; bits::NUM_NODE_PTRS] }
             }
    }

    //
    // Pair of functions to do serialize/deserialize to/from backend
    //
    pub fn read(level: DataType, idx: u32) -> Self {
        println!("Gotta read a node {} {:?}", idx, level);
        let contents = backend::read_data(level, idx, None);
        Node { level,
               idx,
               lookup: deserialize(&contents).unwrap()
             }
    }

    pub fn write(&self) {
        let contents = serialize(&self.lookup).unwrap();
        backend::write_data(&contents, self.level, self.idx, None);
    }

    //
    // Pair of functions to insert/lookup from internal map
    //
    pub fn get(&self, lba: u32) -> Option<u32> {
        let hash = bits::convert_lba(self.level, lba);
        match self.lookup.array[hash] {
            bits::SENTINEL => None,
            val => Some(val)
        }
    }

    pub fn set(&mut self, lba: u32, ptr: u32) {
        let hash = bits::convert_lba(self.level, lba);
        self.lookup.array[hash] = ptr;
    }

    // Two functions to get a child node, one panics if no child (for reads)
    //  the other creates a child and adds it to self (for writes)
    pub fn get_child_or_panic(&self, lba: u32) -> Self {
        let id = self.get(lba).unwrap_or_else(|| panic!("Tried to look up {:?}, {:?} and it failed!", self.level, lba));
        let child_level = self.level.get_child_level();

        Node::read(child_level, id)
    }

    pub fn get_child_or_make(&mut self, lba: u32, default_id: &mut u32) -> Self {
        let ret_node: Node;
        let child_level = self.level.get_child_level();

        if let Some(id) = self.get(lba) {
            ret_node = Node::read(child_level, id);
        }
        else {
            let child_id = *default_id;
            *default_id += 1;
            ret_node = Node::new(child_level, child_id);
            self.set(lba, child_id);
            self.write();
        }
        ret_node
    }
}

//----------------------------------
// -- VLB Stuff
//----------------------------------

pub struct VLB {
    pub idx: u32,
    pub on_disk: VLBOnDisk
}

impl VLB {

    pub fn new(idx: u32, plb_idx: u32) -> Self {
        VLB { idx,
              on_disk: VLBOnDisk { plb_idx, lookup: HashMap::new() }
            }
    }

    pub fn read (idx: u32) -> Self {
        let contents = backend::read_data(DataType::Vlb, idx, None);
        VLB { idx,
              on_disk: deserialize(&contents).unwrap()
            }
    }
    pub fn write(&self, idx: u32) {
        let contents = serialize(&self.on_disk).unwrap();
        backend::write_data(&contents, DataType::Vlb, idx, None);
    }

    pub fn set(&mut self, lba: u32, block: Block) {
        self.on_disk.lookup.insert(lba, block);
    }
}

#[derive(Serialize, Deserialize)]
pub struct VLBOnDisk {
    pub plb_idx: u32,
    pub lookup: HashMap<u32, Block>
}

#[derive(Serialize, Deserialize)]
pub struct Block {
    offset: u32,
    length: u32, // for now always blocksized b/c no compression
    refcount: u32
}

impl Block {
    pub fn new(offset: u32, length: u32, refcount: u32) -> Self {
        Block {offset, length, refcount}
    }
}
