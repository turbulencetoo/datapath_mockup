#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate bincode;

mod tree;
mod backend;
mod bits;
mod data_type;
mod node;

fn main() {
    println!("Hello, world!");
    tree::test();
}
