#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum DataType {
    Top,
    Mid,
    Leaf,
    Vlb,
    Plb
}

impl DataType {
    pub fn get_child_level(self) -> Self {
        match self {
            DataType::Top => DataType::Mid,
            DataType::Mid => DataType::Leaf,
            _ => panic!("Only support top->mid and mid->leaf lookup!!")
        }
    }
}