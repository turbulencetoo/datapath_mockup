use std::u32;

use data_type::DataType;

const MASK_BITS: u32 = 5;
const NUM_TIERS: u32 = 3;
// An LBA can be split into 3 chunks used for tops, mids, leaves eg. 00110 10111 01111
const LBA_BITS: u32 = MASK_BITS * NUM_TIERS;

pub const NUM_NODE_PTRS: usize = 1 << MASK_BITS; // How many children each parent has

const LBA_MAX: u32 = (1 << LBA_BITS) - 1;

pub const BLOCK_SIZE: u32 = 4096;
pub const PLB_SIZE: u32 = 2 * 1024 * 1024;  // 2MB TODO make this smaller for our mockup?

// total number of PLBS available
pub const NUM_PLBS: u32 = (LBA_MAX * BLOCK_SIZE) / PLB_SIZE;

// Masks
const LEAF_MASK: u32 = (1 << MASK_BITS) - 1;
const MID_MASK: u32 =  LEAF_MASK << MASK_BITS;
const TOP_MASK: u32 =  LEAF_MASK << (MASK_BITS * 2);

pub const SENTINEL: u32 = u32::MAX;


// returns a 5 bit number based on which level we're masking
pub fn convert_lba(level: DataType, lba: u32) -> usize {
    match level {
        DataType::Top  => ((lba & TOP_MASK) >> (MASK_BITS * 2)) as usize,
        DataType::Mid  => ((lba & MID_MASK) >> MASK_BITS) as usize,
        DataType::Leaf => (lba & LEAF_MASK) as usize,
        _ => panic!("can only convert lba for tops, mids, leaves!")
    }
}